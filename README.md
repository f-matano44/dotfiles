# dotfiles
MIT License (c) 2025 f-matano44
I use on mint & macOS.


## Dependency
* fish
* pyenv (python 3.11)
* vscodium

### macOS
* Homebrew
* Java 21
* rbenv


## How to setup
```sh
git clone https://gitlab.com/f-matano44/dotfiles.git
cd ./dotfiles/ && sh ./setup.sh
```


## Included file(s)
[Lucario](https://github.com/raphamorim/lucario) (dotfiles/vim/lucario.vim)<br>
**Licence**: MIT (c) [Raphael Amorim](https://github.com/raphamorim).<br>
